﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CsQuery;
using FoodFetcher.Interfaces;

namespace FoodFetcher.Handlers
{
    public class SjorokScraper : IScraper
    {
        private const string SJOROK_URL = "http://www.sjorok.se/";

        public WeekMenu GetMenu()
        {
            string html = FetchPage();
            return Scrape(html);
        }

        private string FetchPage()
        {
            try
            {
                using (WebClient wc = new WebClient())
                {
                    wc.Encoding = Encoding.UTF8;
                    return wc.DownloadString(SJOROK_URL);
                }
            }
            catch (Exception e)
            {
                return string.Empty;
            }
        }

        private WeekMenu Scrape(string html)
        {
            html = System.Web.HttpUtility.HtmlDecode(html);
            var dom = CQ.Create(html);
            var elem = dom[".cff-text"];

            WeekMenu wm = new WeekMenu();
            wm.Resturant = "Sjörök";

            // Decoded menu
            string menu = System.Web.HttpUtility.HtmlDecode(elem.FirstElement().InnerHTML.Replace("<br>", " "));

            // Get week
            string week = menu.Substring(menu.ToLower().IndexOf("vecka") + 6);
            week = week.Remove(2);
            wm.WeekNr = Convert.ToInt32(week);

            // Get monday
            string monday = menu.Substring(menu.ToLower().IndexOf("måndag") + 6);
            monday = monday.Remove(monday.ToLower().IndexOf("tisdag"));

            // Get tuesday
            string tuesday = menu.Substring(menu.ToLower().IndexOf("tisdag") + 6);
            tuesday = tuesday.Remove(tuesday.ToLower().IndexOf("onsdag"));

            // Get wednesday
            string wednesday = menu.Substring(menu.ToLower().IndexOf("onsdag") + 6);
            wednesday = wednesday.Remove(wednesday.ToLower().IndexOf("torsdag"));

            // Get thursday
            string thursday = menu.Substring(menu.ToLower().IndexOf("torsdag") + 7);
            thursday = thursday.Remove(thursday.ToLower().IndexOf("fredag"));

            // Get friday
            string friday = menu.Substring(menu.ToLower().IndexOf("fredag") + 6);
            friday = friday.Remove(friday.ToLower().IndexOf("veckans "));

            wm.Menu.Add(DayOfWeek.Monday, new MenuItem(DayOfWeek.Monday));
            wm.Menu[DayOfWeek.Monday].Food.Add(monday.Trim());

            wm.Menu.Add(DayOfWeek.Tuesday, new MenuItem(DayOfWeek.Tuesday));
            wm.Menu[DayOfWeek.Tuesday].Food.Add(tuesday.Trim());

            wm.Menu.Add(DayOfWeek.Wednesday, new MenuItem(DayOfWeek.Wednesday));
            wm.Menu[DayOfWeek.Wednesday].Food.Add(wednesday.Trim());

            wm.Menu.Add(DayOfWeek.Thursday, new MenuItem(DayOfWeek.Thursday));
            wm.Menu[DayOfWeek.Thursday].Food.Add(thursday.Trim());

            wm.Menu.Add(DayOfWeek.Friday, new MenuItem(DayOfWeek.Friday));
            wm.Menu[DayOfWeek.Friday].Food.Add(friday.Trim());

            return wm;
        }
    }
}
