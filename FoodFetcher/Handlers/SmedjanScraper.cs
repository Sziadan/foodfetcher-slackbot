﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CsQuery;
using FoodFetcher.Interfaces;

namespace FoodFetcher.Handlers
{
    public class SmedjanScraper : IScraper
    {
        private const string SMEDJAN_URL = "http://restaurangmatsmedjan.se/veckans-meny.aspx";

        public WeekMenu GetMenu()
        {
            string html = FetchPage();
            return Scrape(html);
        }

        private string FetchPage()
        {
            try
            {
                using (WebClient wc = new WebClient())
                {
                    wc.Encoding = Encoding.UTF8;
                    return wc.DownloadString(SMEDJAN_URL);
                }
            }
            catch (Exception e)
            {
                return string.Empty;
            }
        }

        private WeekMenu Scrape(string html)
        {
            html = System.Web.HttpUtility.HtmlDecode(html);
            var dom = CQ.Create(html);
            var candidates = dom[".Module-3240.Textarea"];

            WeekMenu wm = new WeekMenu();
            wm.Resturant = "Smedjan";

            int f_counter = 4;

            DayOfWeek currentDay = DayOfWeek.Sunday;
            foreach (var c in candidates.Children())
            {
                string innerText = System.Web.HttpUtility.HtmlDecode(c.InnerText);
                string dayElement = "";
                var day = c.LastElementChild;
                if (day != null)
                {
                    try
                    {
                        dayElement = System.Web.HttpUtility.HtmlDecode(day.InnerText);
                    }
                    catch(Exception e)
                    {

                    }
                }
                dayElement = dayElement.Replace("\t", "").Replace("\n", "").ToLower();
                innerText = innerText.Replace("\t", "").Replace("\n", "");

                if (wm.WeekNr == -1)
                {
                    if (innerText.Contains("v. "))
                    {
                        string weekText = Regex.Split(c.InnerText, "v. ")[1];
                        weekText = weekText.Trim();
                        wm.WeekNr = Convert.ToInt32(weekText);
                    }
                }
                else
                {
                    if (dayElement.Contains("måndag"))
                    {
                        currentDay = DayOfWeek.Monday;
                        f_counter = 0;
                    }
                    else if (dayElement.Contains("tisdag"))
                    {
                        currentDay = DayOfWeek.Tuesday;
                        f_counter = 0;
                    }
                    else if (dayElement.Contains("onsdag"))
                    {
                        currentDay = DayOfWeek.Wednesday;
                        f_counter = 0;
                    }
                    else if (dayElement.Contains("torsdag"))
                    {
                        currentDay = DayOfWeek.Thursday;
                        f_counter = 0;
                    }
                    else if (dayElement.Contains("fredag"))
                    {
                        currentDay = DayOfWeek.Friday;
                        f_counter = 0;
                    }
                    else if (f_counter < 3)
                    {
                        if (!wm.Menu.ContainsKey(currentDay))
                            wm.Menu.Add(currentDay, new MenuItem(currentDay));
                        wm.Menu[currentDay].Food.Add(innerText);
                        f_counter++;
                    }
                }
            }

            return wm;
        }
    }
}
