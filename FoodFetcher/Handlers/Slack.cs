﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using FoodFetcher.Interfaces;

namespace FoodFetcher.Handlers
{
    public class Slack : ISlack
    {
        private string SLACK_URL = string.Empty;

        public Slack(string url)
        {
            this.SLACK_URL = url;
        }

        public bool Send(string message, string channel)
        {
            try
            {
                using (WebClient wc = new WebClient())
                {
                    wc.Encoding = Encoding.UTF8;
                    wc.UploadString(string.Format("{0}&channel=%23{1}&parse=full", SLACK_URL, channel), message);
                    return true;
                }
            }
            catch(Exception e)
            {
                return false;  
            }                     
        }
    }
}
