﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodFetcher
{
    public class WeekMenu
    {
        public WeekMenu()
        {
            WeekNr = -1;
            Menu = new Dictionary<DayOfWeek, MenuItem>();
        }

        public int WeekNr { get; set; }
        public string Resturant { get; set; }
        public Dictionary<DayOfWeek, MenuItem> Menu { get; set; }

        public string PrintMenu()
        {
            string s = $"*{this.Resturant} (v. {WeekNr})*" + Environment.NewLine + Environment.NewLine;
            foreach (var m in Menu)
            {
                s += m.Value.Day.ToSwe() + Environment.NewLine + "```";
                s += m.Value.MenuAsText() + "```" + Environment.NewLine;
            }
            return s;
        }

        public string PrintMenu(DayOfWeek day)
        {
            string s = $"*{this.Resturant} (v. {WeekNr})*" + Environment.NewLine + Environment.NewLine;
            foreach (var m in Menu)
            {
                if (m.Value.Day == day)
                {
                    s += m.Value.Day.ToSwe() + Environment.NewLine + "```";
                    s += m.Value.MenuAsText() + "```" + Environment.NewLine;
                }
            }
            return s;
        }
    }

    public class MenuItem
    {
        public MenuItem()
        {
            Food = new List<string>();
        }

        public MenuItem(DayOfWeek day)
        {
            Food = new List<string>();
            Day = day;
        }

        public DayOfWeek Day { get; set; }
        public List<string> Food { get; set; }

        public string MenuAsText()
        {
            return "* " + string.Join(Environment.NewLine + "* ", Food);
        }
    }

    public static class SwedishDays
    {
        public static string ToSwe(this DayOfWeek day)
        {
            switch(day)
            {
                case DayOfWeek.Monday:
                    return "Måndag";
                case DayOfWeek.Tuesday:
                    return "Tisdag";
                case DayOfWeek.Wednesday:
                    return "Onsdag";
                case DayOfWeek.Thursday:
                    return "Torsdag";
                case DayOfWeek.Friday:
                    return "Fredag";
                case DayOfWeek.Saturday:
                    return "Lördag";
                case DayOfWeek.Sunday:
                    return "Söndag";
                default:
                    return "Måndag";
            }
        }
    }
}
