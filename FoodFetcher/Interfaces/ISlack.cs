﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodFetcher.Interfaces
{
    public interface ISlack
    {
        bool Send(string message, string channel);
    }
}
