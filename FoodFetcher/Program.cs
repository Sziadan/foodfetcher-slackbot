﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FoodFetcher.Interfaces;
using FoodFetcher.Handlers;
using System.IO;

namespace FoodFetcher
{
    class Program
    {
        static void Main(string[] args)
        {
            // If weekend, just exit
            if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday || DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
                return;

            List<WeekMenu> Menu = new List<WeekMenu>();
            List<IScraper> scrapers = new List<IScraper>();
            scrapers.Add(new SmedjanScraper());
            //scrapers.Add(new SjorokScraper());

            foreach (var s in scrapers)
                Menu.Add(s.GetMenu());

            string MessageToSend = "Här kommer dagens lunch!" + Environment.NewLine;

            foreach(var m in Menu)
                MessageToSend += m.PrintMenu(DateTime.Now.DayOfWeek) + Environment.NewLine + Environment.NewLine;

            string url_file = "slack_url.txt";
            if(!File.Exists(url_file))
            {
                Console.WriteLine("Missing url file (slack_url.txt).");
                return;
            }

            string url = File.ReadLines(url_file).First();
            if(string.IsNullOrEmpty(url))
            {
                Console.WriteLine("slack_url.txt didn't contain a slack url.");
                return;
            }

            ISlack slack = new Slack(url);
            slack.Send(MessageToSend, "lunch");
            Console.WriteLine("Sent to slack");
        }
    }
}
